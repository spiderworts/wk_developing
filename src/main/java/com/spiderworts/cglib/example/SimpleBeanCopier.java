package com.spiderworts.cglib.example;

/**
 * 使用cglib做BeanCopier，并保留基本的扩展点
 */
public class SimpleBeanCopier<F,T> extends BeanCopier<F,T> {
	public SimpleBeanCopier(Class<F> sourceClass, Class<T> targetClass){
		setSourceClass(sourceClass);
		setTargetClass(targetClass);
		init();
	}
}
