package com.spiderworts.cglib.example;

/**
 * 使用cglib做BeanCopier，并保留基本的扩展点
 */
public class ConfigurableBeanCopier<F,T> extends BeanCopier<F,T> {

	@SuppressWarnings("unchecked")
	public void setTargetClassName(String targetClass) throws ClassNotFoundException {
		setTargetClass((Class<T>) Class.forName(targetClass));
	}

	@SuppressWarnings("unchecked")
	public void setSourceClassName(String sourceClass) throws ClassNotFoundException {
		setSourceClass((Class<F>) Class.forName(sourceClass));
	}
}
