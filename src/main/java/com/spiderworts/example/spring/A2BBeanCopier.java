package com.spiderworts.example.spring;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Service;

import com.spiderworts.cglib.example.BeanCopier;
import com.spiderworts.example.beans.A;
import com.spiderworts.example.beans.B;

@Service
public class A2BBeanCopier extends BeanCopier<A,B> {

	@PostConstruct
	public void init(){
		setSourceClass(A.class);
		setTargetClass(B.class);
		super.init();
	}

	@Override
	public B postCopy(A source, B target) {
		target.setF5("aaa");
		return target;
	}
}
