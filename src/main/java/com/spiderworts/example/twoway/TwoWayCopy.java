package com.spiderworts.example.twoway;

import java.util.List;

import com.google.common.collect.Lists;
import com.spiderworts.cglib.example.BeanCopier;
import com.spiderworts.cglib.example.SimpleBeanCopier;
import com.spiderworts.example.beans.A;
import com.spiderworts.example.beans.B;

public class TwoWayCopy {

	public static void main(String[] args) {
		List<A> listA;
		List<B> listB;
		listA = Lists.newArrayList();
		listB = Lists.newArrayList();

		BeanCopier<A, B> beanCopier = new SimpleBeanCopier<A, B>(A.class, B.class);
		listB = Lists.transform(listA, beanCopier);
		listA = Lists.transform(listB, beanCopier.reverse());
	}
}
