package com.spiderworts.reflection.util;

import com.spiderworts.reflection.copier.BeanCopier;

public abstract class BeanCopiers {

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static <A, B> BeanCopier<A, B> copier(Class<A> clazzA, Class<B> clazzB) {
		return new BeanCopier(clazzA, clazzB);
	}
}
