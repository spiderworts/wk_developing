package com.spiderworts.reflection.util;

import com.spiderworts.reflection.copier.BeanCopier;

public class BeanTransformer<F,T> extends BeanCopier<F,T> {

	public BeanTransformer(Class<F> clazzFrom, Class<T> clazzTo) {
		super(clazzFrom, clazzTo);
	}

	@Override
	public T copy(F f, T t) {
		return doBusiness(f, super.copy(f, t));
	}

	public T doBusiness(F f, T t) {
		return t;
	}
}
