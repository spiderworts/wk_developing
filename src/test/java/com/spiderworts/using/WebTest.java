package com.spiderworts.using;

import java.util.Date;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import com.spiderworts.util.working.DateUtil;

public class WebTest {

	protected Logger LOG = Logger.getLogger(getClass());
	
	@Test
	public void testNullCharacterLength() {
		String nullStr = "";
		String[] nullStrArr = nullStr.split(",");
		Assert.assertEquals(1, nullStrArr.length);
	}
	
	@Test
	public void testDateMethods() {
		Date startDate = DateUtil.getMomentDate("2015-08-09", DateUtil.MOMENT_START);
		Date endDate = DateUtil.getMomentDate("2015-08-10", DateUtil.MOMENT_START);
		Assert.assertEquals(1, DateUtil.getDistDates(startDate, endDate));
		
		startDate = DateUtil.getMomentDate("2015-08-01", DateUtil.MOMENT_START);
		endDate = DateUtil.getMomentDate("2015-08-31", DateUtil.MOMENT_START);
		Assert.assertEquals(30, DateUtil.getDistDates(startDate, endDate));
	}
	
	@Test
	public void testZeroIsEqualLongZero() {
		Long zeroObj = 0L;
		long zero = 0;
		Assert.assertFalse("0".equals(zeroObj));
		Assert.assertFalse("0".equals(zero));
	}
}
