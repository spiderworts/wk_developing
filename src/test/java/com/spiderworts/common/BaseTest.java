package com.spiderworts.common;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

public class BaseTest {

	protected Logger LOG = Logger.getLogger(getClass());
	
	@Test
	public void testZeroIsEqualLongZero() {
		Long zeroObj1 = 0L;
		long zero = 0;
		Assert.assertFalse("0".equals(zeroObj1));
		Assert.assertFalse("0".equals(zero));
		Assert.assertTrue("0".equals(zeroObj1.toString()));
		Assert.assertTrue(zeroObj1.equals(zero));
		Assert.assertTrue(0 == zero);
		Assert.assertTrue(0 == zeroObj1);
		
		Long zeroObj2 = 0L;
		Assert.assertTrue(zeroObj2.equals(zeroObj1));
		Assert.assertFalse(zeroObj2.equals(zeroObj1.toString()));
		Assert.assertFalse(zeroObj2.toString().equals(zeroObj1));
		Assert.assertTrue(zeroObj2.toString().equals(zeroObj1.toString()));
		
		Long zeroObj3 = new Long(0);
		Assert.assertTrue(zeroObj3.equals(zeroObj1));
		Assert.assertTrue(0 == zeroObj3);
	}
	
	@Test
	public void testIsEqualOfTwoInteger() {
		Integer a1 = new Integer(10);
		Integer a2 = new Integer(10);
		Integer a3 = 10;
		Integer a4 = 10;
		Assert.assertFalse(a1 == a2);
		Assert.assertFalse(a1 == a4);
		Assert.assertTrue(a3 == a4);
	}
	
	@Test
	public void testAllNullObjectInList() {
		List<String> a = new ArrayList<String>(); 
		a.add("123");
		a.add(null);
		Assert.assertTrue(2 == a.size());
		a.add(null);
		Assert.assertTrue(3 == a.size());
	}
}
